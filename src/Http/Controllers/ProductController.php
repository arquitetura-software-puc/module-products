<?php

namespace Mgzaspuc\Products\Http\Controllers;

use App\Http\Controllers\Controller;
use Mgzaspuc\Products\Http\Requests\UpdateRequest;
use Mgzaspuc\Products\Http\Requests\StoreRequest;
use Mgzaspuc\Products\Products;
use Mgzaspuc\Providers\Providers;
use Intervention\Image\Facades\Image;

class ProductController extends Controller
{
    const STORAGE_FILE_UPLOAD = 'upload/img/products';
    const FILE_HEIGHT = '250';
    const FILE_WIDTH = '250';
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = new Products();
        $listProducts = $products->paginate(15);
        
        return view('products.index', compact('listProducts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $provider = new Providers();
        $listProvider = $provider->all();
        return view('products.create', compact('listProvider'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        try{        
            $product = new Products();
            $product->name = $request->get('name');
            $product->photo = $this->executeUploadPhoto($request);
            $product->description = $request->get('description');
            $product->price = $this->formatPriceToDatabase($request->get('price'));
            $product->price_discount = $this->formatPriceToDatabase($request->get('price_discount'));
            $product->amount = $request->get('amount');
            $product->id_provider = $request->get('id_provider');
            $product->created_at = new \DateTime();
            $product->updated_at = new \DateTime();
            $product->save();
        
            return redirect('/produtos')
                ->with('success', 'Registro criado com sucesso!');            
            
        } catch (\Exception $ex) {
            return redirect('/produto/novo')
                ->with('error', 'Não foi possível criar o registro!' . $ex->getMessage())
                ->withInput($request->input());
        }
    }
    
    private function executeUploadPhoto($request) : string
    {
        if(!$request->hasFile('photo')){
            throw new \Exception('Deve-se informar a foto do Produto');
        }
        
        $photo = $request->file('photo');

        if(!$photo->isValid()){
            throw new \Exception('A foto do produto informado é inválido');
        }

        if(!in_array($photo->extension(), ['png', 'jpg', 'jpeg'])){
            throw new \Exception('A foto do produto deve ser no formato png, jpg ou jpeg');
        }

        $photoName = sprintf('%s_%s.%s', uniqid(), date('dmYhis'), 
            $photo->extension());
        
        $pathToFile = storage_path(
            'app/public/' . 
            $this::STORAGE_FILE_UPLOAD . 
            DIRECTORY_SEPARATOR . 
            $photoName
        );                 
        
        $image = Image::make($photo->getRealPath());    

        $wasUploaded = $image->resize($this::FILE_WIDTH, $this::FILE_HEIGHT, function ($constraint) {
            $constraint->aspectRatio();
        })->save($pathToFile);      

        if(!$wasUploaded){
            throw new \Exception('Não foi possível realizar upload da foto do produto');
        }        
        
        return $photoName;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $products = new Products();
        $product = $products->find($id);
        
        $provider = new Providers();
        $listProvider = $provider->all();
        
        return view('products.edit', compact('product', 'listProvider'));    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request)
    {   
        try{
            $namePhotoFile = $request->get('name_photo_old');
            
            if($request->hasFile('photo')){
                $namePhotoFile = $this->executeUploadPhoto($request);
                
                $fileToRemove = storage_path(
                    'app/public/' . 
                    $this::STORAGE_FILE_UPLOAD . 
                    DIRECTORY_SEPARATOR . 
                    $request->get('name_photo_old')
                ); 
                
                if(file_exists($fileToRemove) && !is_dir($fileToRemove)){
                    unlink($fileToRemove);
                }
            }
            
            $products = new Products();
            $product = $products->find($request->input('id'));
            $product->name = $request->get('name');
            $product->photo = $namePhotoFile;
            $product->description = $request->get('description');
            $product->price = $this->formatPriceToDatabase($request->get('price'));
            $product->price_discount = $this->formatPriceToDatabase($request->get('price_discount'));
            $product->amount = $request->get('amount');
            $product->id_provider = $request->get('id_provider');
            $product->updated_at = new \DateTime();
            $product->save();
        
            return redirect('/produtos')
                ->with('success', 'Registro alterado com sucesso!');            
            
        } catch (\Exception $ex) {
            return redirect('/produto/editar/' . $request->input('id'))
                ->with('error', 'Não foi possível alterar o registro!');
        }
    }  
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try{
            $products = new Products();
            $product = $products->find($request->input('id'));        
            $product->delete();       
            
            return redirect('/produtos')
                ->with('success', 'Registro excluído com sucesso!');            
            
        } catch (\Exception $ex) {
            return redirect('/produtos')
                ->with('error', 'Não foi possível excluir o registro!');
        }
    }
    
    public function show($id)
    {
        $products = new Products();
        $product = $products->find($id);
        
        $provider = new Providers();
        $listProviders = $provider->all();
        
        return view('products.show', compact('product', 'listProviders'));          
    }
    
    private function formatPriceToDatabase($price)
    {
        return str_replace(['.', ','], ['', '.'], $price);        
    }
}
