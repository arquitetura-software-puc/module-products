<?php

namespace Mgzaspuc\Products\Http\Controllers;

use Illuminate\Http\Request;
use Mgzaspuc\Products\Products;
use App\Http\Controllers\Controller;

class IntegrationController extends Controller {

    public function getProductProvider1() 
    {
        $client = new \SoapClient(null, array(
            'location' => "http://service.murillozampieri.com/webservice_soap/server.php",
            'uri' => "http://localhost/webservice_soap",
            'trace' => 1));

        $productList = $client->__soapCall("getListProduct", []);
    
        $now = \Carbon\Carbon::now();
        $this->save($productList, 1);
        $this->removeOldProducts(1,$now);
    }

    public function getProductProvider2() 
    {
        $serviceProviderRest = file_get_contents('http://service.murillozampieri.com/webservice_rest.php');
        $productList = json_decode($serviceProviderRest);

        $listProduct = [];
        foreach ($productList as &$product) {
            $listProduct[] = (array) $product;
        }

        $now = \Carbon\Carbon::now();
        $this->save($listProduct, 2);
        $this->removeOldProducts(2,$now);

    }

    private function save(array $productList, $providerId) 
    {
        foreach ($productList as $productData) {
            if ($productData) {
                $product = new Products();
                $product->name = $productData['name'];                
                $product->description = $productData['description'];                
                $product->price = $productData['price'];                
                $product->price_discount = $productData['price_discount'];                
                $product->amount = $productData['amount'];                
                $product->id_provider = $providerId;
                $product->created_at = new \DateTime();
                $product->updated_at = new \DateTime();
                $product->save();
            }
        }
    }

    private function removeOldProducts($idProvider, $dateLimit) 
    {
        Products::where('id_provider', $idProvider)->where('created_at', '<', $dateLimit->format('Y-m-d H:i:s'))->delete();
    }
}
