@extends('layouts.admin')
@section('content')
<div id="heading-breadcrumbs">
    <div class="container">
        <div class="row d-flex align-items-center flex-wrap">
            <div class="col-md-7">
                <h1 class="h2">Produtos</h1>
            </div>
            <div class="col-md-5">
                <ul class="breadcrumb d-flex justify-content-end">
                    <li class="breadcrumb-item"><a href="{{url('/admin')}}">Dashboard</a></li>
                    <li class="breadcrumb-item active">Produtos</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div id="content">
    <div class="container">
        <div class="row bar">
            <div class="col-md-3">
                <!-- MENUS AND FILTERS-->
                <div class="panel panel-default sidebar-menu">
                    <div class="panel-heading">
                        <h3 class="h4 panel-title">Menu</h3>
                    </div>
                    <div class="panel-body">
                        @include('elements.admin_menu')
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <h2>Cadastrar Produto</h2>
                @include('elements.message_success_error')

                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
                @endif
                <form method="post" action="{{ route('produto_salvar') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">    
                        <label for="name">Nome:</label>
                        <input type="text" class="form-control" id="name" name="name" value="{{old('name')}}"/>
                    </div>

                    <div class="form-group">    
                        <label for="photo">Imagem do produto:</label>
                        <input type="file"id="photo" name="photo" class="form-control" style="padding: 0" />
                        <small style="color: red">A foto do produto deve ser no formato png, jpg ou jpeg e na dimensão 250x250 pixels</small>
                    </div>                    
                    
                    <div class="form-group">    
                        <label for="id_provider">Fornecedor:</label>
                        <select id="id_provider" name="id_provider" class="form-control">
                            <option name="">Selecione</option>  
                            @foreach($listProvider as $provide)
                                @if($provide->id == old('id_provider'))
                                <option selected value="{{$provide->id}}">{{$provide->trade_name}}</option>  
                                @else
                                <option value="{{$provide->id}}">{{$provide->trade_name}}</option>  
                                @endif
                            @endforeach
                        </select>
                    </div>                    
                    
                    <div class="form-group">
                        <label for="description">Descrição:</label>
                        <textarea class="form-control" id="description" name="description">{{old('description')}}</textarea>                        
                    </div>

                    <div class="form-group">
                        <label for="price">Preço:</label>
                        <input type="text" class="form-control" id="price" name="price" value="{{old('price')}}"/>
                    </div>
                    <div class="form-group">
                        <label for="price_discount">Preço com desconto:</label>
                        <input type="text" class="form-control" id="price_discount" name="price_discount" value="{{old('price_discount')}}"/>
                    </div>
                    <div class="form-group">
                        <label for="amount">Quantidade:</label>
                        <input type="int" class="form-control" id="amount" name="amount" value="{{old('amount')}}"/>
                    </div>                    
                    <button type="submit" class="btn btn-default">Cadastrar</button>
                </form>
            </div>                
        </div>
    </div>
</div>
</div>
<!-- GET IT-->
@endsection

@section('script')
<script type="text/javascript">
    $('[name="price"]').mask('000.000.000.000.000,00', {reverse: true});
    $('[name="price_discount"]').mask('999.999.999.999.990,00', {reverse: true});
</script>
@endsection('script')