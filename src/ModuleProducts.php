<?php

namespace Mgzaspuc\Products;

use Illuminate\Support\ServiceProvider;

class ModuleProducts extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/migrations' => base_path('database/migrations/')
        ]);

        $this->publishes([
            __DIR__.'/resources' => base_path('resources/views/')
        ]);
    }
}