@extends('layouts.admin')
@section('content')
<div id="heading-breadcrumbs">
    <div class="container">
        <div class="row d-flex align-items-center flex-wrap">
            <div class="col-md-7">
                <h1 class="h2">Produtos</h1>
            </div>
            <div class="col-md-5">
                <ul class="breadcrumb d-flex justify-content-end">
                    <li class="breadcrumb-item"><a href="{{url('/admin')}}">Dashboard</a></li>
                    <li class="breadcrumb-item active">Produtos</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div id="content">
    <div class="container">
        <div class="row bar">
            <div class="col-md-3">
                <!-- MENUS AND FILTERS-->
                <div class="panel panel-default sidebar-menu">
                    <div class="panel-heading">
                        <h3 class="h4 panel-title">Menu</h3>
                    </div>
                    <div class="panel-body">
                        @include('elements.admin_menu')
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                @include('elements.message_success_error')
                <a href="{{url('/produto/novo/')}}" class="btn btn-sm btn-template-main float-right">Novo produto</a>
                <br /><br />
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nome</th>
                                <th>Preço</th>
                                <th>Preço com desconto</th>
                                <th>Cadastro</th>
                                <th>Alteração</th>
                                <th>Ação</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($listProducts as $product)
                            <tr>
                                <td>{{$product->id}}</td>                                    
                                <td>{{$product->name}}</td>
                                <td>{{number_format($product->price, 2, ',', '.')}}</td>
                                <td>{{number_format($product->price_discount, 2, ',', '.')}}</td>
                                <td>{{$product->created_at->format('d/m/Y')}}</td>
                                <td>{{$product->updated_at->format('d/m/Y')}}</td>
                                <td class="actions">
                                    <div class="btn-group">
                                        <a href="{{url('/produto/editar/' . $product->id)}}" class="btn btn-info btn-sm" title="Editar"><i class="fa fa-pencil fa-align-center"></i></a>                                        
                                        <form name="post_{{$product->id}}" style="display:none;" method="post" action="{{url('/produto/excluir/')}}">
                                            @csrf
                                            <input type="hidden" name="id" value="{{$product->id}}">
                                        </form>
                                        <a href="#" class="btn btn-danger btn-sm" title="Excluir" onclick="if (confirm('Tem certeza que deseja excluir o registro # {{$product->id}}?')) {
                                                    document.post_{{$product->id}}.submit();
                                                }
                                                event.returnValue = false;
                                                return false;">
                                            <i class="fa fa-trash fa-align-center"></i>
                                        </a>                                   
                                    </div>                                
                                </td>                                        
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    
                    {{ $listProducts->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
<!-- GET IT-->
@endsection
