<?php

namespace Mgzaspuc\Products\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:191',
            'id_provider' => 'required|numeric',
            'description' => 'required',            
            'price' => 'required|regex:/^\d{1,3}(?:\.\d{3})*?,\d{2}/',
            'price_discount' => 'required|regex:/^\d{1,3}(?:\.\d{3})*?,\d{2}/',
            'amount' => 'required|numeric|min:1'
        ];
    }

    public function messages() {
        return [
            'name.required' => 'O campo Nome do Produto não pode ser vazio',
            'name.max' => 'O Nome do Produto não pode ter mais de 191 caracteres',
            'id_provider.required' => 'O Fornecedor deve ser informado',
            'id_provider.numeric' => 'O Fornecedor deve ser informado',
            'id_provider.numeric' => 'O Fornecedor deve ser informado',
            'description.required' => 'A Descrição do Produto deve ser informado',
            'price.required' => 'O Preço do Produto deve ser informado',
            'price.regex' => 'O Preço do Produto deve ser no formato 00,00 ou 0.000,00',
            'price_discount.required' => 'O Preço com desconto do Produto deve ser informado',
            'price_discount.regex' => 'O Preço com desconto do Produto deve ser no formato 00,00 ou 0.000,00',            
            'amount.required' => 'A Quantidade do Produto deve ser informado',
            'amount.numeric' => 'A Quantidade do Produto deve ser um número',
            'amount.min' => 'A Quantidade do Produto mínima deve ser 1',
        ];   
    }
}
