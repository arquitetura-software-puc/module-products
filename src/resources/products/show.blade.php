@extends('layouts.commerce')
@section('content')
<div id="heading-breadcrumbs">
    <div class="container">
        <div class="row d-flex align-items-center flex-wrap">
            <div class="col-md-7">
                <h1 class="h2">{{$product->name}}</h1>
            </div>
            <div class="col-md-5">
                <ul class="breadcrumb d-flex justify-content-end">
                    <li class="breadcrumb-item"><a href="{{url('/home')}}">Home</a></li>
                    <li class="breadcrumb-item active">Produto</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div id="content">
    <div class="container">
        <div class="row bar">
            <div class="col-md-3">
                <!-- MENUS AND FILTERS-->
                <div class="panel panel-default sidebar-menu">
                    <div class="panel-heading">
                        <h3 class="h4 panel-title">Marcas</h3>
                    </div>
                    <div class="panel-body">
                        <ul class="nav nav-pills flex-column text-sm category-menu">
                            @foreach($listProviders as $item)
                            <li class="nav-item">
                                <a href="{{url(sprintf('/home/marca/%s/%s', $item->id, str_replace(' ', '-', strtolower($item->trade_name))))}}" class="nav-link d-flex align-items-center justify-content-between">
                                    <span>{{$item->trade_name}}</span>
                                </a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-5">
                        <div data-slider-id="1" class="owl-carousel shop-detail-carousel owl-loaded owl-drag">
                            <div class="owl-stage-outer">
                                <div class="owl-stage" style="transform: translate3d(-795px, 0px, 0px); transition: all 0.25s ease 0s; width: 1193px;"><div class="owl-item" style="width: 397.5px;">
                                        <div> 
                                            @if($product->photo)
                                            <img src="{{url("storage/upload/img/products/{$product->photo}")}}" alt="{{$product->name}}" class="img-fluid image1" />
                                            @else
                                            <img src="{{url("img/product_default.jpg")}}" alt="{{$product->name}}" class="img-fluid image1" />                                        
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <h3>Tamanhos disponíveis</h3>
                        <div class="dropdown bootstrap-select bs-select">
                            <select class="bs-select" tabindex="-98">
                                <option value="small">Pequeno</option>
                                <option value="medium">Médio</option>
                                <option value="large">Grande</option>
                            </select> 
                        </div>

                        <br /><br />

                        <button type="submit" class="btn btn-template-outlined">
                            <i class="fa fa-shopping-cart"></i> Adicionar ao carrinho
                        </button>

                        <button type="submit" data-toggle="tooltip" data-placement="top" title="" class="btn btn-default" data-original-title="Adicionar aos favoritos">
                            <i class="fa fa-heart-o"></i>
                        </button>


                        <br /><br />

                        <h2>R$ {{number_format($product->price, 2, ',', '.')}}</h2>
                    </div>

                    <div class="col-md-9">
                        <h4>Detalhes do produto</h4>
                        <p>{{$product->description}}</p>
                    </div> 

                    <div class="col-md-9">
                        <br /><br />
                        <h4 class="heading-light">Compartilhe em suas redes sociais</h4>
                        <ul class="social list-inline">
                            <li class="list-inline-item"><a href="#" data-animate-hover="pulse" class="external facebook"><i class="fa fa-facebook"></i></a></li>
                            <li class="list-inline-item"><a href="#" data-animate-hover="pulse" class="external gplus"><i class="fa fa-google-plus"></i></a></li>
                            <li class="list-inline-item"><a href="#" data-animate-hover="pulse" class="external twitter"><i class="fa fa-twitter"></i></a></li>
                            <li class="list-inline-item"><a href="#" data-animate-hover="pulse" class="email"><i class="fa fa-envelope"></i></a></li>
                        </ul>
                    </div> 
                </div>
            </div>
        </div>
    </div>
</div>
<!-- GET IT-->
@endsection
