@extends('layouts.admin')
@section('content')
<div id="heading-breadcrumbs">
    <div class="container">
        <div class="row d-flex align-items-center flex-wrap">
            <div class="col-md-7">
                <h1 class="h2">Produtos</h1>
            </div>
            <div class="col-md-5">
                <ul class="breadcrumb d-flex justify-content-end">
                    <li class="breadcrumb-item"><a href="{{url('/admin')}}">Dashboard</a></li>
                    <li class="breadcrumb-item active">Produtos</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div id="content">
    <div class="container">
        <div class="row bar">
            <div class="col-md-3">
                <!-- MENUS AND FILTERS-->
                <div class="panel panel-default sidebar-menu">
                    <div class="panel-heading">
                        <h3 class="h4 panel-title">Menu</h3>
                    </div>
                    <div class="panel-body">
                        @include('elements.admin_menu')
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <h2>Alterar Produto</h2>
                @include('elements.message_success_error')

                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
                @endif
                <form method="POST" action="{{ route('produto_atualizar') }}" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="id" value="{{$product->id}}" />
                    <input type="hidden" name="name_photo_old" value="{{$product->photo}}" />
                    
                    <div class="form-group">
                        <img src="{{url("storage/upload/img/products/{$product->photo}")}}" alt="{{$product->name}}" class="img img-thumbnail img-fluid" />
                    </div>
                    
                    <div class="form-group">    
                        <label for="name">Nome:</label>                        
                        <input type="text" class="form-control" id="name" name="name" value="{{$product->name}}"/>
                    </div>
                    
                    <div class="form-group">    
                        <label for="photo">Imagem do produto:</label>
                        <input type="file"id="photo" name="photo" class="form-control" style="padding: 0" />
                        <small style="color: red">A foto do produto deve ser no formato png, jpg ou jpeg e na dimensão 250x250 pixels</small>
                    </div>                    

                    <div class="form-group">    
                        <label for="id_provider">Fornecedor:</label>
                        <select id="id_provider" name="id_provider" class="form-control">
                            <option name="">Selecione</option>  
                            @foreach($listProvider as $provide)
                                <option value="{{$provide->id}}" @if($product->id_provider == $provide->id) selected="true" @endif>
                                    {{$provide->trade_name}}
                                </option>  
                            @endforeach
                        </select>
                    </div>                    
                    
                    <div class="form-group">
                        <label for="description">Descrição:</label>
                        <textarea class="form-control" id="description" name="description">{{$product->description}}</textarea>                        
                    </div>

                    <div class="form-group">
                        <label for="price">Preço:</label>
                        <input type="text" class="form-control" id="price" name="price" value="{{number_format($product->price, 2, ',', '.')}}"/>
                    </div>
                    <div class="form-group">
                        <label for="price_discount">Preço:</label>
                        <input type="text" class="form-control" id="price_discount" name="price_discount" value="{{number_format($product->price_discount, 2, ',', '.')}}"/>
                    </div>
                    <div class="form-group">
                        <label for="amount">Quantidade:</label>
                        <input type="int" class="form-control" id="amount" name="amount" value="{{$product->amount}}"/>
                    </div>                    
                    <button type="submit" class="btn btn-default">Alterar</button>
                </form>
            </div>                
        </div>
    </div>
</div>
</div>
<!-- GET IT-->
@endsection

@section('script')
<script type="text/javascript">
    $('[name="price"]').mask('000.000.000.000.000,00', {reverse: true});
    $('[name="price_discount"]').mask('999.999.999.999.990,00', {reverse: true});
</script>
@endsection('script')