<?php

namespace Mgzaspuc\Products;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $table = 'products';

    public function provider() {
        return $this->belongsTo('Mgzaspuc\Providers\Providers','id_provider','id');
    }
}
